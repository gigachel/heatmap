(function () {

  function Heatmap(options) {
    var self = this;
    this.redClickCount = '2%'; // или процент ('2%') или число (5) кликов до покраснения пикселей
    this.pixelRadius = 20;
    this.documentWidth = document.body.clientWidth;
    this.documentHeight = document.body.clientHeight;
    this.clicksMap = {};
    this.storage = options.storage;
    this.targets = options.targets || 'body';

    this._canvas = document.createElement('canvas');
    this._canvas.style.position = 'absolute';
    this._canvas.style.top = '0';
    this._canvas.style.left = '0';
    this._canvas.style.display = 'none';
    this._canvas.style.zIndex = '9999';
    this._canvas.width = this.documentWidth;
    this._canvas.height = this.documentHeight;
    document.body.appendChild(this._canvas);

    [].forEach.call(document.querySelectorAll(this.targets), function(item) {
      item.addEventListener('click', function (e) {
        // e.stopPropagation();
        self.storage.add([e.pageX, e.pageY]);
        // console.log(e.pageX, e.pageY);
      });
    });
  }

  Heatmap.prototype = { 

    ratePixel: function (x, y) { // чем дальше пиксель от центра - тем меньше очков он получит
      var startRow = y - this.pixelRadius < 0 ? 0 : y - this.pixelRadius;
      var startCol = x - this.pixelRadius < 0 ? 0 : x - this.pixelRadius;
      var endRow = y + this.pixelRadius > this.documentHeight ? this.documentHeight : y + this.pixelRadius;
      var endCol = x + this.pixelRadius > this.documentWidth ? this.documentWidth : x + this.pixelRadius;

      for (var i = startCol; i <= endCol; i++) { // x
        for (var j = startRow; j <= endRow; j++) { // y
          var ratePixelRadius = Math.sqrt(Math.pow(x - i, 2) + Math.pow(y - j, 2));
          if (ratePixelRadius < this.pixelRadius) {
            if (!this.clicksMap[i]) this.clicksMap[i] = {};
            if (!this.clicksMap[i][j]) this.clicksMap[i][j] = 0;
            this.clicksMap[i][j] = this.clicksMap[i][j] + (this.pixelRadius - Math.round(ratePixelRadius));
          }
        }
      }
    },

    drawPixel: function (canvasData, x, y, r, g, b, a) {
      var index = (x + y * this.documentWidth) * 4;

      canvasData.data[index + 0] = r;
      canvasData.data[index + 1] = g;
      canvasData.data[index + 2] = b;
      canvasData.data[index + 3] = a;
    },

    hsl2rgb: function (h, s, l) {
      var a = s * Math.min(l, 1 - l);
      var f = function f(n) {
        var k = (n + h / 30) % 12;
        return l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      };
      return [f(0) * 255, f(8) * 255, f(4) * 255];
    },

    draw: function () {      
      var ctx = this._canvas.getContext('2d');
      this._canvas.style.display = '';

      var pageClicks = this.storage.get() || [];
      for (var i = 0; i < pageClicks.length; i++) {
        this.ratePixel(pageClicks[i][0], pageClicks[i][1]);
      }
      var canvasData = ctx.getImageData(0, 0, this.documentWidth, this.documentHeight);
      
      var isPercent = String(this.redClickCount).indexOf('%') !== -1;
      var clickCount = this.redClickCount;
      if (isPercent) {
        var percent = Number(this.redClickCount.replace('%', ''));
        clickCount = pageClicks.length * percent / 100;
      }
      
      var xArr = Object.keys(this.clicksMap);
      for (var k = 0; k < xArr.length; k++) { // перебираем все пиксели, которым присвоили очки
        var yArr = Object.keys(this.clicksMap[xArr[k]]);
        for (var l = 0; l < yArr.length; l++) {
          var maxValue = clickCount * this.pixelRadius; // максимальное число очков, которое может получить пиксель
          var colorRate = this.clicksMap[xArr[k]][yArr[l]] < maxValue ? this.clicksMap[xArr[k]][yArr[l]] : maxValue; // очки конкретного пикселя
          var rgb = this.hsl2rgb(240 - (colorRate * 240 / maxValue), 1, .5); // в hsl линейный переход от синего к красному
          var alfa = colorRate * 240 / maxValue; // увеличиваем прозрачность к перефирийным пикселям
          this.drawPixel(canvasData, Number(xArr[k]), Number(yArr[l]), rgb[0], rgb[1], rgb[2], alfa);
        }
      }

      ctx.putImageData(canvasData, 0, 0);   
    },

    clear: function () {
      var ctx = this._canvas.getContext('2d');
      ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
      this._canvas.style.display = 'none';
      this.clicksMap = {};
    }
  }

  function Storage (name) {
    this.name = name;
    if (!localStorage.getItem(this.name)) localStorage.setItem(this.name, JSON.stringify([]));
    this.storageIsNotEmptyEvent = document.createEvent('Event');
    this.storageIsNotEmptyEvent.initEvent('storageIsNotEmpty', true, true);
    if (JSON.parse(localStorage.getItem(this.name)).length) document.dispatchEvent(this.storageIsNotEmptyEvent);
  }  

  Storage.prototype = {

    add: function (item) {      
      try {
        var storedItems = JSON.parse(localStorage.getItem("clicks_" + location.pathname));
        storedItems.push(item);
        localStorage.setItem(this.name, JSON.stringify(storedItems));
        document.dispatchEvent(this.storageIsNotEmptyEvent);
      } catch (error) {
        if (error === QUOTA_EXCEEDED_ERR) {
          console.log('Превышен лимит localStorage');
        } else {
          console.log(error, 'error');
        }
      }
    },

    get: function () {
      return JSON.parse(localStorage.getItem(this.name));
    }
  }

  function HeatmapBtn(options) {
    var self = this;
    var storage = options.storage;
    this.primaryAction = true;
    
    this.setButtonText = function () {
      if (self.primaryAction) {
        heatmapBtn.innerText = 'Показать тепловую карту ' + storage.get().length;
      } else {
        heatmapBtn.innerText = 'Скрыть тепловую карту ' + storage.get().length;
      }
    }

    var heatmapBtn = document.createElement('button');
    heatmapBtn.style.position = 'fixed';
    heatmapBtn.style.display = storage.get().length ? '' : 'none';
    heatmapBtn.style.top = '30px';
    heatmapBtn.style.right = '30px';
    heatmapBtn.style.userSelect = 'none';
    heatmapBtn.style.zIndex = '10000';
    heatmapBtn.innerText = 'Показать тепловую карту ' + storage.get().length;   

    heatmapBtn.addEventListener('click', function (e) {
      e.stopPropagation();
      if (self.primaryAction) options.primaryAction();
      else options.secondaryAction();        
      self.primaryAction = !self.primaryAction;
      self.setButtonText();
    });

    document.addEventListener('storageIsNotEmpty', function (e) {
      heatmapBtn.style.display = '';
      self.setButtonText();
    });

    document.body.appendChild(heatmapBtn);
  }

  ////////////////////////////////////////////////////////////////////

  var storage = new Storage("clicks_" + location.pathname);
  
  var heatmap = new Heatmap({
    storage: storage,
    // targets: 'a, button',
  });

  var heatmapBtn = new HeatmapBtn({
    storage: storage,
    primaryAction: function () {
      heatmap.draw();      
    },
    secondaryAction: function () {
      heatmap.clear();      
    }
  });

})();
